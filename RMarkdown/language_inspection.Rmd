---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
``` 




# Libs
```{r}

library(here)
library(sf)
library(areal)
 
library(patchwork)
library(dodgr)
library(tmap)
library(snapbox)
 

library(cancensus)
library(Census2SfSp)


library(magrittr)
library(tidyverse) 
library(glue)
library(reshape2)


R.utils::sourceDirectory(here("R"))

 

```

#Params
```{r}

cancensus_api_key <- keyringr::decrypt_gk_pw('cancensus_api_key key_value')
options(cancensus.api_key = cancensus_api_key)
options(cancensus.cache_path = "/home/charles/Projects/Census2SfSp/Cache")

mapBoxToken <- keyringr::decrypt_gk_pw('token mapToken')
Sys.setenv(MAPBOX_ACCESS_TOKEN=mapBoxToken)
 

proj32198 <- '+proj=lcc +lat_1=60 +lat_2=46 +lat_0=44 +lon_0=-68.5 +x_0=0 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs'


use_hardcoded_regions <- F

```



---
---


# Census inspection

 
 
## Regions

```{r}

if(use_hardcoded_regions){
    
  cities <- c('Halifax', "Québec","Montréal",'Vancouver', 'Ottawa','Toronto') #order the cities in prop of total visible minorities
  
  city_str <- paste(cities, collapse='$|^') #exact city name -- e.g. no North Vancouver
  
  print(glue('Considering the following hardcoded cities: {city_str}' ))
    
  
  census_regions <- list_census_regions('CA16') %>% 
    filter(grepl( glue('^{city_str}$'),name,perl = T) & level == 'CSD' & municipal_status != 'P') #last one is a hack for Moncton
  
  # census_regions_cma <- list_census_regions('CA16') %>% 
  #   filter(grepl( glue('^{city_str}$'),name,perl = T) & level == 'CMA')
  
  stopifnot(nrow(census_regions)==length(cities))
  #stopifnot(nrow(census_regions_cma)==length(cities))

  
}else {
  
# Top 50 CMAs in terms of pop
thresh_pop <- 50*10**3 

census_regions <- list_census_regions('CA16') %>% 
  filter( level == 'CSD' & pop > thresh_pop)  %>%
  arrange(desc(pop)) %>% 
  distinct(name,.keep_all = T) %>% 
  top_n(n=50,wt=pop)

cities <- census_regions %>% pull(name)

}


```



## Language vectors
```{r}


languages <- c('English','French','Non-official languages')

list_census_vectors('CA16') %>% filter( grepl('[mM]other',details) & grepl('Total',type)) 

map_dfr( languages,
    ~ list_census_vectors('CA16') %>% filter(grepl(glue('^{.x}$'), label) & grepl('[mM]other',details) & grepl('Total',type)) %>% select(vector , type, label, details,parent_vector ) 
    )

```

---
---

# Census data

## Get the census data for official languages -- mother tongue
```{r}

shp_census_languages <- get_census(dataset = 'CA16',
           level = 'DA',
           vectors = c('v_CA16_557','v_CA16_560','v_CA16_554', 'v_CA16_551','v_CA16_563'), #use v_CA16_551 -- single responsed instead of 548 -- Total - Mother tongue for the total population excluding institutional residents - 100% data	
           regions = list(CSD=census_regions$region) ,
           geo_format = 'sf' )
 
colnames(shp_census_languages) [ colnames(shp_census_languages) %in% c("v_CA16_557: English", "v_CA16_560: French", "v_CA16_554: Official languages", 'v_CA16_551: Single responses', 'v_CA16_563: Non-official languages') ] <- c('English','French','all_official_languages','all_mother_tongues', 'Non-official languages')

shp_census_languages %>% head
```


# Basic data engineering

## Prop of speakers
```{r}

cols_language_official <- c("English", "French")
shp_census_languages %<>% mutate_at( .vars = all_of(cols_language_official), 
                                     .funs = list( prop_official_languages= ~.x/all_official_languages  ) )
 
shp_census_languages %<>% mutate_at( .vars = all_of(languages), 
                                     .funs = list(   prop_mother_tongue = ~.x/all_mother_tongues ) )


#Some errors  perhaps issues with single response
# GeoUID  48111161   10 english speakers and 5 speakers of official languages
prop_cols  <- grep('prop', colnames(shp_census_languages), value=T)
shp_census_languages %<>% mutate_at( .vars = all_of(prop_cols),
                                     .funs = ~pmin(.x,1) 
)
shp_census_languages %<>% mutate_at( .vars = all_of(prop_cols),
                                     .funs = ~pmax(.x,0) 
)
assertthat::are_equal(T, shp_census_languages$French_prop_mother_tongue %>% max(na.rm = T) <= 1)
assertthat::are_equal(T, shp_census_languages$French_prop_mother_tongue %>% min(na.rm = T) >= 0)

shp_census_languages %>% head
```



### Add province name

```{r}



# Get the intersection of all censuses
pwd <- keyringr::decrypt_gk_pw( glue("user charles"))

 
con <- DBI::dbConnect(RPostgres::Postgres(),
                          dbname = 'census',
                          host='localhost',
                          user='charles',
                          password=pwd
  )
  
  queryStr <- 'SELECT * FROM province_names'


df_provinces <- DBI::dbGetQuery(con,
                              statement  = queryStr)  %>% 
  mutate(provCode = as.character(provCode) ) %>% 
  rename(prov_name=name)
  
shp_census_languages %<>% mutate( PR_UID = substr(GeoUID,1,2))
shp_census_languages %<>% left_join(df_provinces, by= c("PR_UID" = "provCode" )  )

```


## Split by city
```{r}

shp_census_languages_by_city <- map(cities,
                                       ~shp_census_languages %>% filter(`Region Name` == .x ) ) %>% 
  set_names(cities)


```

---
---

# Plots

## Density


### By DA + all mother tongue
```{r}

cols_language <- grep(pattern = 'prop_mother_tongue', x= colnames(shp_census_languages), value = T)
cols <- c(cols_language,"Region Name", 'GeoUID' )

shp_melted_by_da <- shp_census_languages %>% 
  select(all_of(cols)) %>% 
  pivot_longer(cols=cols_language ,names_to='language') %>% 
  mutate(language = str_replace(language, '_prop_mother_tongue','') ) %>% 
  rename(city= `Region Name`)

p <- ggplot(shp_melted_by_da) + 
  geom_density(aes(x=value, fill=city), alpha=0.5) + 
  facet_wrap(~language ) +
  theme_bw()+ 
  ggtitle('Mother tongue in select census subdivisions (CSD)')  +
  labs(subtitle = 'Distribution at the dissemination area (DA) level', 
       caption = 'Considers individuals reporting a single mother tongue') + 
  xlab('Proportion of speakers - by mother tongue')  

if( !use_hardcoded_regions){
  p <- p + theme(legend.position = 'none') # cannot make out a legend with 100 entries
}

(p)

language_str <- paste(languages, collapse = "_") %>% str_replace(pattern = " ", replacement = "_")


ggsave(here('Figures', 'Bar_density_plots' ,glue('{language_str}_mother_tongue_by_city_harcoded_{use_hardcoded_regions}.png')),
       p)

```


### By DA + oficial language
```{r}

cols_language <- grep(pattern = 'prop_official_languages', x= colnames(shp_census_languages), value = T)
cols <- c(cols_language,"Region Name", 'GeoUID' )

shp_melted_by_da <- shp_census_languages %>% 
  select(all_of(cols)) %>% 
  pivot_longer(cols=cols_language ,names_to='language') %>% 
  mutate(language = str_replace(language, '_prop_official_languages','') ) %>% 
  rename(city= `Region Name`)

p <- ggplot(shp_melted_by_da) + 
  geom_density(aes(x=value, fill=city), alpha=0.5) + 
  facet_wrap(~language) +
  theme_bw()+ 
  ggtitle('Mother tongue in select census subdivisions (CSD)')  +
  labs(subtitle = 'Distribution per dissemination area', 
       caption = 'Considers individuals reporting a single mother tongue') + 
  xlab('Proportion of speakers - by official language')


if( !use_hardcoded_regions){
  p <- p + theme(legend.position = 'none') # cannot make out a legend with 100 entries
}


(p)

ggsave(here('Figures', 'Bar_density_plots' , glue('{language_str}_off_language_by_cityharcoded_{use_hardcoded_regions}.png')) ,
       p)
```


### By entire city + all mother tongue
```{r}

 
df_by_city <- shp_census_languages %>%
  st_set_geometry(NULL) %>% 
  group_by(`Region Name`, region) %>% 
  summarise_at( .vars = all_of(languages), 
             .funs = list( prop_official_languages= ~sum(.x,na.rm = T)/sum(all_official_languages,na.rm = T),
                           prop_mother_tongue = ~sum(.x,na.rm = T)/sum(all_mother_tongues,na.rm = T) ) ) %>% 
  dplyr::arrange( desc( English_prop_mother_tongue)  )
   

cols_language <- grep(pattern = 'prop_mother_tongue', x= colnames(df_by_city), value = T)
cols <- c(cols_language,"Region Name" ,"region" )

df_by_city_melted <-  df_by_city %>% 
  select(all_of(cols)) %>% 
  pivot_longer(cols=cols_language ,names_to='language') %>% 
  mutate(language = str_replace(language, '_prop_mother_tongue','') ) %>% 
  rename(city= `Region Name`) %>%
  mutate(city=factor(city, levels=as.character(df_by_city$`Region Name`) ))



p <- ggplot(df_by_city_melted) + 
  geom_col(aes(y=value, x=city,fill=region), alpha=0.5) + 
facet_wrap(~language ,nrow = 3) +
  theme_bw()+ 
  #theme( axis.text.x =  element_text(angle=45)) + 
  theme(legend.position = 'bottom') + 
  ggtitle('Mother tongue in select census subdivisions (CSD)')  +
  labs(subtitle = 'Distribution per city', 
       caption = 'Considers individuals reporting a single mother tongue.\n50 largest CSDs with population greater or equal to 50,000') + 
  ylab('Proportion of speakers - by mother tongue') + 
  theme(axis.text.x = element_text(angle=90))


if( use_hardcoded_regions){
   p <- p+ geom_label( aes(y=value, label=round(value,2), x=city)) 
}

(p)

ggsave(here('Figures','Bar_density_plots'   ,glue('{language_str}_mother_tongue_col_plot_by_city_harcoded_{use_hardcoded_regions}.png')) ,
       p,
       width=12,
       height = 7)

df_by_city_melted
```

## Maps
```{r}


plot_eng_by_city <- function(shp, 
                             col_to_plot='English_prop_mother_tongue', 
                             col_to_plot_new_name=NULL,
                             min_val=0,
                             max_val=1){
  #Get the city name
  city <- unique(shp$`Region Name`)
 
  col_city_wide <- df_by_city %>% 
    filter(`Region Name` == city) %>% 
    pull(col_to_plot)
  
  #Change name of legend if required
  if(is.null(col_to_plot_new_name)){
    col_to_plot_new_name <- col_to_plot
  }
  
  
  ggplot(  ) + 
    layer_mapbox( map_style = mapbox_light(), st_bbox(shp)) + 
    geom_sf( data=shp, aes_string(fill=col_to_plot) , lwd=0 ) + 
    scale_fill_viridis_c(limits=c( min_val, max_val) ,name=col_to_plot_new_name) + 
    ggtitle(glue("{city}")) + 
    labs(caption = glue('CSD wide: {round(col_city_wide*100,0)}%')) + 
    coord_sf(datum = NA) + 
    theme_bw()  
  
}


```


```{r}



var_old <- "English_prop_mother_tongue"
var_new <- 'Proportion of speakers'

 


min_val <- shp_census_languages[[var_old]] %>% min(na.rm=T)
max_val <- shp_census_languages[[var_old]] %>% max(na.rm=T)

list_plots <- map(shp_census_languages_by_city[1:6],  #only plot the first 6 -- impossible to map much more
                  plot_eng_by_city,
                  col_to_plot=var_old,
                  col_to_plot_new_name=var_new,
                  min_val=min_val,
                  max_val=max_val)


p <- wrap_plots(ncol = 2, list_plots )   +
  plot_layout(guides = "collect",heights = c(1,1,1)) &
  theme(legend.position = "right") &
  plot_annotation(title = 'Speakers with English as mother tongue',
                  subtitle = 'Select census subdivision',
                   caption = 'Census subdivision boundaries (CSD) shown.\nData at the dissemination area (DA) level.\nSource: Statistic Canada through {cancensus}.\nBasemap: mapbox through {snapbox}' ) 




ggsave(here('Figures', 'Maps' ,glue('map_{var_old}_use_hardcoded_regions_{use_hardcoded_regions}.png')),
       p)



```