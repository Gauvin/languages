---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
  


# Libs
```{r}

library(here)
library(sf)
library(areal)
 
library(patchwork)
library(dodgr)
library(tmap)
library(snapbox)

library(cancensus)
library(Census2SfSp)

library(magrittr)
library(tidyverse) 
library(glue)
library(reshape2)

```

#Params
```{r}

cancensus_api_key <- keyringr::decrypt_gk_pw('cancensus_api_key key_value')
options(cancensus.api_key = cancensus_api_key)
options(cancensus.cache_path = "/home/charles/Projects/Census2SfSp/Cache")

mapBoxToken <- keyringr::decrypt_gk_pw('token mapToken')
Sys.setenv(MAPBOX_ACCESS_TOKEN=mapBoxToken)
 

proj32198 <- '+proj=lcc +lat_1=60 +lat_2=46 +lat_0=44 +lon_0=-68.5 +x_0=0 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs'


```


# Which language vectors should we consider?
```{r}
 
use_mother <-  F #100% data
use_spoken_home <- T #100% data for 2016, but 20% for 2001
use_spoken_work <- F #25% data fpror 2016

geo_hierarchy <- 'CD' #'CD' # 'CD' #

geo_hierarchy_str <- NA
if(geo_hierarchy=='CSD'){
  geo_hierarchy_str <- 'census subdivision'
}else if(geo_hierarchy=='CD'){
  geo_hierarchy_str <- 'census division'
}else if(geo_hierarchy=='CMA' ){
    geo_hierarchy_str <- 'census metropolitan area'
}


```


---
---


# Census inspection


## Regions

```{r}

cities <- c("Québec","Montréal",'Ottawa','Toronto','Vancouver','Greater Vancouver', 'Halifax') #allow greater vancouver for CD

df_cma <- data.frame(CMA_UID=c("12205" ,"24421", "24462", "35535" ,"505" ,  "59933"),
                     `Region Name`=c('Halifax','Quebec','Montreal','Toronto','Ottawa','Vancouver' )) 
colnames(df_cma) <- c("CMA_UID","Region Name")

city_str <- paste(cities, collapse='$|^') #exact city name -- e.g. no North Vancouver
census_regions <- list_census_regions('CA16') %>% 
  filter(grepl( glue('^{city_str}$'),name,perl = T) & level == geo_hierarchy  & municipal_status != 'P') #last one is a hack for Moncton


stopifnot(nrow(census_regions)==length(cities)-1)
```


## Languages and years
```{r}



# Make sure exactly a single param is chosen
stopifnot(use_mother + use_spoken_home + use_spoken_work == 1)

if(use_mother){
  grep_pattern <- '[mM]other'
  string_name <- 'mother_tongue'
  plot_title_str <- 'Mother tongue'
}else if( use_spoken_home){
  grep_pattern <-  "home"
  string_name <- 'home'
  plot_title_str <- 'Tongue used most often at home'
}else if( use_spoken_work){
  grep_pattern <-  "at work"
  string_name <- 'work'
  plot_title_str <- 'Tongue used most often at work'
}else{
  stop('Fatal error! invalid param!')
}

```



## Language vectors
```{r}


languages <- c('English','French','Non-official languages')
language_col_mapping <- c("English" = "red", "French" = "dodgerblue", 'Non-official languages'=  "darkgreen")
years <- c('01','06','11','16')  #96 data is missing


census_laguages <- map_dfr( years,
      ~ list_census_vectors(glue('CA{.x}')) %>% 
        filter(grepl(glue('^English$'), label) & grepl(grep_pattern,details) & grepl('Total',type)) %>% 
        select(vector , type, label, details,parent_vector ) 
      )



#Some hardcoding here  
# for 2016 difference between used & used most often - work
#For 2016 use nly single response
# for 2011 this vector considers multiple responses, rather than just english - work
# etc, etc. 
#single responses kept
list_forbiden_vectors <- c("v_CA11N_1900",
                           "v_CA11N_1963",
                           'v_CA16_6665',
                           'v_CA16_6668',
                           'v_CA11F_929',
                           'v_CA11F_935',
                           'v_CA16_2168' ,
                           'v_CA11F_932',
                           'v_CA16_2171',
                           'v_CA16_548',
                           'v_CA16_1355' ,
                           'v_CA16_5852'
                           )   
census_laguages %<>% filter( !(vector %in% list_forbiden_vectors ) )
  


stopifnot(nrow(census_laguages)==length(years))

```


## Language vectors - utils
```{r}



#' Title
#'
#' @param year 
#' @param grep_pattern determines what kid of variable we wil query - e.g.  '[mM]other' gets the mother tongue
#' @param max_counter 
#'
#' @return
#' @export
#'
#' @examples
get_df_census_vars <- function(year,  
                               grep_pattern = grep_pattern, 
                               max_counter = 10){
  
  df <- map_dfr( languages,
      ~ list_census_vectors(glue('CA{year}')) %>% 
        filter(grepl(glue('^{.x}$'), label) & grepl(grep_pattern,details) & grepl('Total',type)) %>% 
        select(vector , type, label, details,parent_vector ) 
      )
  
  df %<>% filter( !(vector %in% list_forbiden_vectors ) )
   
  #No parent vector : the sums don't add up
  
  return(df)

}

```

## Get all the relevant vectors - English + French + total for normalization
```{r}


df_census_all_years <- map_dfr(years,
        get_df_census_vars,
        grep_pattern = grep_pattern) %>% 
  distinct(vector, .keep_all = T) %>% 
  filter( !(vector %in% list_forbiden_vectors ) )

df_census_all_years %<>% mutate(year= substr( vector, nchar('v_CA')+1, nchar('v_CA')+2 ) )

df_census_all_years %<>% mutate(label  = ifelse(label %in% languages, label, 'Total'))
df_census_all_years %<>% mutate(new_name =paste(label , year ,sep="_") )

df_census_all_years %>% head

#make sure data for all years
stopifnot(nrow(df_census_all_years) == length(years)*(length(languages)))   

```

---
---

# Census data
 

## Get the census data for French -- mother tongue
```{r}
 

shp_census_languages <- get_census(dataset = 'CA16',
           level = 'DA',
           vectors = df_census_all_years$vector,
           regions = list(census_regions$region) %>% set_names(geo_hierarchy) ,
           geo_format = 'sf' ,
           labels = 'short')

#Rename the vectors
list_rename <-  df_census_all_years$new_name %>% set_names (df_census_all_years$vector)

shp_census_languages %<>% plyr::rename( list_rename ) 


shp_census_languages %>% head
```

## Melt by census year
```{r}

shp_census_languages_melt_year <- shp_census_languages %>%
  select(c(CMA_UID, GeoUID, df_census_all_years$new_name)) %>% 
  pivot_longer(cols=(df_census_all_years$new_name)) %>% 
  mutate(year = substr(name, nchar(name)-1,nchar(name) )) %>% 
  mutate(language = str_extract(name, pattern = '.*(?=_)')) %>% 
  st_as_sf()

```

## GIS sanity check
```{r}
library(tmap)
tm_shape(shp_census_languages)+ 
  tm_borders() + 
tm_facets(by=c("CMA_UID"), ncol  =1, showNA = FALSE)


ggplot(shp_census_languages_melt_year %>% filter(CMA_UID== "24462" ) %>% filter( grepl("Non-official",name)) ) + 
  geom_sf(aes(fill=value),lwd=0) + 
  facet_wrap(~year)

```


# Compute totals (over DAs) by language for each census year and city
```{r}



df_melt_year <- shp_census_languages_melt_year %>%  
  st_set_geometry(NULL) %>% 
  group_by(CMA_UID,year ,language ) %>% 
  summarise( total = sum(value,na.rm=T))

#Hack to get the correct total
df_melt_total <- shp_census_languages_melt_year %>%  
  st_set_geometry(NULL) %>% 
  group_by(CMA_UID,year) %>% 
  summarise( total = sum(value,na.rm=T)) %>% 
  mutate(language="Total")

df_melt_year <- rbind(df_melt_year, df_melt_total) %>%
  arrange(CMA_UID,year)

 
#Make sure the totals add up - could be caused by some sampling issue
df_wide_check <- df_melt_year %>% ungroup %>% pivot_wider( names_from='language' , values_from='total')
df_wide_check$Total_check <- df_wide_check %>% select(English,French,`Non-official languages`) %>% rowSums()
df_wide_check %<>% mutate(diff=Total_check-Total)

assertthat::assert_that( max( abs(df_wide_check$diff)) <= 10**-4 )

```

# Rectangularise to compute proportion
```{r}

df_rect <- df_melt_year %>% 
  pivot_wider(names_from ="language" , values_from ="total")

df_rect %<>% mutate_at( vars(languages), 
                       .funs = list(proportion = ~.x/Total)
)

df_rect


df_rect %<>% mutate( prop_total = English_proportion+ French_proportion+  `Non-official languages_proportion` )

#Remove NAs - prob small cities
df_rect %<>% drop_na()

assertthat::assert_that( max( abs( 1-df_rect$prop_total) ) <= 5*10**-2 )#error is not over 5%

```


# Remelt for plotting
```{r}

cols_prop <- paste0(languages , '_proportion')

df_melt_plot <- df_rect %>% 
  pivot_longer(cols=cols_prop ) %>% 
  mutate(name = str_replace(name, "_proportion", ""))

#Add back the CMA
df_melt_plot %<>% left_join( df_cma, by='CMA_UID')

df_melt_plot %>% head



```

---
---


# Plots

```{r}


p <- ggplot(df_melt_plot  ) + 
  geom_line(aes(x=year,y=value,col=name,group=name),key_glyph = "timeseries") + 
  scale_color_manual(name="", values = language_col_mapping) + 
  facet_wrap(~`Region Name`) + 
  hrbrthemes::theme_ipsum_rc() + 
  theme(legend.position = 'bottom',
        plot.subtitle=element_text(color='darkgrey') ) + 
  ylab("Proportion") + 
  ggtitle(glue("{plot_title_str} by census year and city") )+
  labs(subtitle='Single responses',
       caption=glue('Source: Statistics Canada through [cancensus].\nData aggregated over the entire {geo_hierarchy_str} ({geo_hierarchy}).\nPlot by @charles_gauvin'),
       col='Language')


(p)

language_str <- paste(languages, collapse = "_") %>% str_replace(pattern = " ", replacement = "_")

ggsave(here('Figures', 'Time_Series' , glue('{language_str}_{string_name}_time_series_{geo_hierarchy}_v2.png')),
       p,
       width=7,
       height=7)

```